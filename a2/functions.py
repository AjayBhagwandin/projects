def get_requirements():
 
  
    print("Payroll Calculator\n")
    print("Program Requirements: \n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        + "4. Must format currency with dollar sign, and round to two decimal places.\n"
        + "5. Create at least three functions that are called by the program\n"
        +"\ta. main(): calls at least two other functions.\n"
        +"\tb. get_requirements(): displays the program requirements.\n"
        +"\ta. calculate_payroll(): calculates an individual one-week paycheck.\n")

def calculate_payroll():

    hours, holiday_hr, rate = get_input()

    base_hr, overtime_hr= check_overtime(hours)
    
    base_py = base_hr * rate
    overtime_py = overtime_hr * (rate * 1.5)
    holiday_py = holiday_hr * (rate * 2)

    gross_pay = base_py + overtime_py + holiday_py
    print_payroll(base_py, overtime_py, holiday_py, gross_pay)
    

def print_payroll(base,overtime,holiday,gross):
 
    print("\nOutput:\n"
        + "Base:     " + format_to_money(base) + "\n"
        + "Overtime: " + format_to_money(overtime) + "\n"
        + "Holiday:  " + format_to_money(holiday) + "\n"
        + "Gross:    " + format_to_money(gross))
    pass

def get_input():
   
    print("Input:")
    hr_worked = float_handler("Enter hours worked: ")
    holiday_hr = float_handler("Enter holiday hours: ")
    pay_rate = float_handler("Enter hourly pay rate: ")

    return hr_worked, holiday_hr, pay_rate

def float_handler(phrase):
  
    try:
        return float(input(phrase))
    except:
        print("!!Please enter a numerical value!!\n")
        return float_handler(phrase)

def check_overtime(hours):
   
    if(hours > 40):
        overtime_hr = hours - 40
        base_hr = 40
    else:
        base_hr = hours
        overtime_hr = 0

    return base_hr, overtime_hr
    
def format_to_money(number):
  
    decimalPlaces = "{0:,.2f}".format(number)
    money = '$%s' % decimalPlaces
    return money