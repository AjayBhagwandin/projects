> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Interprise Solutions

## Ajay Bhagwandin

### LIS4369 Requirements:

*Sub-Heading:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
>- Install AMMPS
>- Provide screenshots of installtions
>- Create Bitbucket repo
>- Compete bitbucket tutorial
>- Provide git command descriptions
2.  [A2 README.md](a2/README.md "My A2 README.md file")
>- Backwards-engineer (using Python) 
>- The program should be organzied with two moduels
>- Test with IDLE and Visual Studio Code
3. [A3 README.md](a3/README.md "My A3 README.md file")
>-  Backwards-engineer (using Python) 
>- The program should be organzied with two moduels
>- Test with IDLE and Visual Studio Code
>- Use Float, SQFT_PER_GALLON
>-use iternation structure
4.  [A4 README.md](a4/README.md "My A4 README.md file")
>-  Backwards-engineer (using Python) 
>- The program should be organzied with two moduels
>- Test with IDLE and Visual Studio Code
>- Use dat_analysis_2 and pip freeze
5.  [A5 README.md](a5/README.md "My A5 README.md file")
>-  Backwards-engineer (using R) 
>- The program test various graphs 
>- Includes atleast two plots 
6.  [P1 README.md](p1/README.md "My p1 README.md file")
>-  Backwards-engineer (using Python) 
>- The program should be organzied with two moduels
>- Test with IDLE and Visual Studio Code
>- Use pandas, matplotlib, pandas-datareader
>-used 3 functions; main(), get_requirments, data_analysis
7.  [P2 README.md](p2/README.md "My p2 README.md file")
>-  Backwards-engineer (using R) 
>- Use motor trend car road test
>- research data
