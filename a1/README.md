> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ajay Bhagwandin

### Assignment 1 Requirements:

*Steps:*

>1. Version control with Git and bitbucket
>2. Create tutorial
>3. Questions

#### README.md file should include the following items:

>* Screenshots of a1 Tip calculator
>* Git command description
* 
* 


> #### Git commands w/short descriptions:

>1. git init - Create git repository
>2. git status - displays working directories
>3. git add - add changes in the directory
>4. git commit - records any file changes
>5. git push - upload to the local repository
>6. git pull - download content from repository
>7. git remote - manages connections to remote repositories

#### Assignment Screenshots:

*Screenshot of IDLE*:

![JDK Installation Screenshot](img/Tip1.png)

*Screenshot of Visual Studio Code - My First App*:

![Android Studio Installation Screenshot](img/Tip2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
