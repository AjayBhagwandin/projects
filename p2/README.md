> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4369

## Ajay Bhagwandin

### Assignment: p2 # Requirements:

*Sub-Heading:*

1. Backwards engineer with R
2. Use head and tail 
3. Use str and names to look 


#### Assignment Screenshots:

*Displacement Vs MPG*:

![AMPPS Installation Screenshot](img/p1.png)

*weight vs MPG*:

![JDK Installation Screenshot](img/p2.png)

*Coding - My First App*:

![Android Studio Installation Screenshot](img/p3.png)

*Coding 2 - My First App*:

![Android Studio Installation Screenshot](img/p4.png)


*Coding 3 - My First App*:

![Android Studio Installation Screenshot](img/p5.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
