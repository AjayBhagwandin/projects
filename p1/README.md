> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ajay Bhagwandin

### Project 1 # Requirements:

*Sub-Heading:*

1. Backwards-engineer using Python
2. Created 3 functions
3. tested with IDLE and Visual studio with a graph



#### Assignment Screenshots:

*Visual Studio*:

![AMPPS Installation Screenshot](img/p1.png)



![JDK Installation Screenshot](img/p2.png)

*IDLE*:

![Android Studio Installation Screenshot](img/p3.png)
![JDK Installation Screenshot](img/p4.png)

*Graph*:
![JDK Installation Screenshot](img/graph.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
