> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4369

## Ajay Bhagwandin

### Assignment 3 # Requirements:

*Steps:*

1. Backwards-engineer (using Python) 
2. The program should be organzied with two moduels
3. Test with IDLE and Visual Studio Code


#### Assignment Screenshots:

*Visutal Studio*:

![AMPPS Installation Screenshot](img/pic1.png)

*Idle*:

![JDK Installation Screenshot](img/pic2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
