> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ajay Bhagwandin

### Assignment 5 # Requirements:

*Requirements:*

1. Complete The intro to R set up tutorial
2. Code and run lis4369_a5
3. Include atleast two plots 



#### Assignment Screenshots:

*Screenshot of boxplot*:

![AMPPS Installation Screenshot](img/P1.png)

*Screenshot BarGraph*:

![JDK Installation Screenshot](img/P2.png)

*Screenshot of Stripchart*:

![Android Studio Installation Screenshot](img/P3.png)


