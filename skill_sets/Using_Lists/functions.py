def get_requirements():
    print("Python Lists.\n")
    print( "Program requirments:\n"
    + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
    + "2. List are mutable/chaneable--that is, can insert, update, delete.\n"
    + "3. Create lists - using square brackets [list]: my_lists = [cherries","apples", "Banana" , "oranges ].\n"
    + "4. Create a program that mirrors the following IPO (input/process/output) format.\n")
def lists():    


    print("\nInput:")
    lists = (input("enter number of list elements "))
    e1 = (input("Please enter list element 1: "))
    e2 = (input("Please enter list element 2: "))
    e3 = (input("Please enter list element 3: "))
    e4 = (input("Please enter list element 4: "))
    lists = [e1,e2,e3,e4]
    print("\nOutput:")
    print("\nPrint my lists:")
    print(lists)
    e5 = (input("Please enter lists elements: "))
    pos = int(input("Please enter lists *index* position (note: must convert to int): "))
    lists.insert(pos,e5)
    print(lists)
    
    print("\nCount number of elements in lists")
    print(len(lists))
    print("\nSort elements in list alphabetically: ")
    lists.sort()
    print(lists)
    print("\nReverse lists: ")
    lists.reverse()
    print(lists)
    print("\nRemove last list elembents: ")
    del lists[-1]
    print(lists)
    print('\nDelete second element from list by *index* (note: 1=2nd element):  ')
    lists.pop(1)
    print(lists)
    print("\nDelete element from list by *value* (cherries): ")
    lists.remove("cherries")
    print(lists)
    print("\nDelete all elements from lists: ")
    lists.clear()
    print(lists)