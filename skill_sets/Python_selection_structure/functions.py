def get_requirements():
    print("Python Selection Structures.\n")
    print( "Program requirments:\n"
    + "1. Use Python selection structure.\n"
    + "2. Promp user for two numbers, and a suitable operator.\n"
    + "3. Test for correct numeric operator.\n"
    + "4. Replicate display below.\n")

def Python_selection_structure():
    print("\nPython calculator:")
    num1 = float(input("Enter num1:"))
    num2 = float(input("Enter num2:"))
    print("\n")
    print("Suitable Operators: +,-,*,/,// (integer division), % (module operator), ** (power) ")
    op = str(input("Enter operator: "))
    
    if (op == "+"):
        result = num1 + num2
        print(result)
    elif (op == "-"):
        result = num1 - num2
        print(result)
    elif (op == "*"):
        result = num1 * num2
        print(result)
    elif (op == "/"):
        result = num1 / num2
        print(result)
    elif (op == "%"):
        result = num1 % num2
        print(result)
    elif (op == "**"):
        result = num1 ** num2
        print(result)
    else:
        print("Incorrect operator!")