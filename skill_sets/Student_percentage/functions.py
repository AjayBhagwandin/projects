def get_requirements():
    print("IT/ICT Student Percentage.\n")
    print( "Program requirments:\n"
    + "1. Find number of IT/ICT students in class.\n"
    + "2. Calculate IT/ICT Student Percentage.\n"
    + "3. Must use float data type (to facilitate right-alighment).\n"
    + "4. Format, right-align numbers, and round to two decimal places.\n")

def calculate_miles_per_gallon():
    total = 0.0
    IT = 0.0
    ICT = 0.0
    ITP = 0.0
    ICTP = 0.0


    print("\nInput: ")
    IT = float(input("Enter number of IT students: "))
    ICT = float(input("Enter number of ICT students: "))

    total = round(IT + ICT, 2)
    ITP = round(IT / total * 1, 4)
    ICTP = round(ICT / total * 1, 4)

    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("total students", total))
    print("{0:17} {1:>5.2%}".format("IT students", ITP))
    print("{0:17} {1:>5.2%}".format("ICT stduents", ICTP))


