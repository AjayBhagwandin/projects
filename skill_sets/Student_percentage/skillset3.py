def get_requirements():
    print( "Program requirments:\n"
    + "1. Research: number of square feet to acres of land.\n"
    + "2. Must use float data type for user input and calulations.\n"
    + "3. Format and round conversion to two decimals places.\n")

def calculate_miles_per_gallon():
    total = 0.0
    IT = 0.0
    ICT = 0.0
    ITP = 0.0
    ICTP = 0.0


    print("\nInput: ")
    IT = float(input("Enter number of IT students: "))
    ICT = float(input("Enter number of ICT students: "))

    total = round(IT + ICT, 2)
    ITP = round(IT / total * 100, 2)
    ICTP = round(ICT / total * 100, 2)

    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("total students", total))
    print("{0:17} {1:>5.2f}".format("IT students", ITP))
    print("{0:17} {1:>5.2f}".format("ICT stduents", ICTP))


