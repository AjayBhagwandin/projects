def get_requirements():
    print("Python Loop Structures.\n")
    print( "Program requirments:\n"
    + "1. print while loop.\n"
    + "2. print for loops using range() function, and implicit and explicit lists.\n"
    + "3. use break and continue statements.\n"
    + "4. replicate display below.\n")

def calculate_python_loops():
    print("\n1. while loop:")
    i = 1
    while i <= 3:
        print(i)
        i = i+1 
    print("\n2. for loop: using range() function with 1 arg")
    for i in range(4):
        print(i)
    print("\n3. for loop: using range() function with 2 arg")
    for i in range(1,4):
        print(i)
    print("\n4. for loop: using range() function with 3 arg (interval 2)")
    for i in range(1,4,2):
        print(i)
    print("\n5. for loop: using range() function with 3 arg (negative interval )")
    for i in range(3,-1,-2):
        print(i)

    print("\n6. for loop: using (implicit) list")
    for i in [1,2,3]:
        print(i)

    print("\n7. for loop: iterating through (explicit) string list: ")
    list1 = ["Michigan", "Alabama", "Florida"]
    for i in list1:
        print(i)

    print("\n8. for loop using break statement (stops loop):")
    for i in list1:
        if i == "FLorida":
            break
        print(i)

    print("\n9. for loop using continue statement (stops and continues with next): ")
    for i in list1:
        if i == "Alabama":
            continue
    print(i)

    print("\n10. print list length:")
    print(len(list1))