def get_requirements():
    print("Calorie Percentage.\n")
    print( "Program requirments:\n"
    + "1. Find calories per grams of fat, carbs, and protein.\n"
    + "2. Calculate  Percentage.\n"
    + "3. Must use float data type.\n"
    + "4. Format, right-align numbers, and round to two decimal places.\n")

def calculate_calories_percentage():
    fat_cal = 0.0
    carb_cal = 0.0
    protein_cal = 0.0
    total = 0.0
    fat_t = 0.0
    carb_t = 0.0
    protein_t = 0.0

    print("\nInput:")
    fat = float(input("Enter total fat grams: "))
    carb = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))

    fat_cal = fat * 9
    carb_cal = carb * 4
    protein_cal = protein * 4
    total = fat_cal + carb_cal + protein_cal
    fat_t = fat_cal / total
    carb_t = carb_cal / total
    protein_t = protein_cal / total

    print("\nOutput:")
    print("{0:17} {1:17} {2:17}".format("Type", "Calories", "Percentage"))
    print("{0:17} {1:,.2f} {2:>19.2%}".format("Fat", fat_cal, fat_t))
    print("{0:17} {1:,.2f} {2:>19.2%}".format("Carbs", carb_cal, carb_t))
    print("{0:17} {1:,.2f} {2:>19.2%}".format("Protein", protein_cal, protein_t))
    print("\n")