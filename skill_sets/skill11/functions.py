def get_requirements():
    print("Pseudo-random Number Generator")
    print(" ")
    print("\nProgram Requirments:\n"
        "1. Dictionaries (Python data structure): unordered key:value pairs.\n"
        "2. Dictionary: an associative array (also known as hashes). \n"
        "3. Any key in a dictionary is associated (or mapped) to a value (i.e., any python data type).\n"
        "4. Keys: must be of immutable type (string, number or tuple with immutable elements) and must be unique.\n"
        "5. Values: can be any data type and can repeat. \n")

def random_numbers():
    start = 0
    end = 0

    print("input: ")
    start = int(input("enter beginning value: "))
    end = int(input("enter ending value: "))

    print("\nOutput:")
    print("Example1: using range() and randint() functions ")
    for count in range(10):
        print(random.randint(start,end),sep=",",end=" ")
    print()

    print("\nExample 2: Using a list, with range () and shuffle() functions: ")

    r = list(range(start,end + 1))
    random.shuffle(r)
    for i in r:
        print(i, sep=", ",end= " ")
    print()





