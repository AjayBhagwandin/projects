> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4369

## Ajay Bhagwandin

### Assignment 4 # Requirements:

*Sub-Heading:*

1. Run demo.py
2. If errors, missing package
3. Research how to install packages


#### Assignment Screenshots:

*Screenshot of graph*:

![AMPPS Installation Screenshot](img/graph.png)

*Idle*:

![JDK Installation Screenshot](img/P1.png)

![JDK Installation Screenshot](img/P1.2.png)
![JDK Installation Screenshot](img/P1.3.png)

*Visual Studio*:

![Android Studio Installation Screenshot](img/P2.png)

![Android Studio Installation Screenshot](img/P2.1.png)

![Android Studio Installation Screenshot](img/P1.4.png)


